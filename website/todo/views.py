from django.shortcuts import render
from django.http import HttpResponse
from todo.models import todo_app

# Create your views here.
def index(request):
    task_list = None
    if request.method == 'POST':
        s = todo_app()
        s.task_name = request.POST['task']
        s.save()
    task_list = todo_app.objects.all()
    context = {'task_list': task_list}
    request.method= "GET"
    request.POST = {}
    return render(request, 'todo_view.html', context)

def del_task(request, id):
    if id == None:
        task_list = todo_app.objects.all()
        context = {'task_list': task_list}
        return render(request, 'todo_view.html', context)
    else:
        t = todo_app.objects.filter(id=id)
        t.delete()
    task_list = todo_app.objects.all()
    context = {'task_list': task_list}
    return render(request, 'todo_view.html', context)

def contact(request):
    return render(request, 'contact.html')
